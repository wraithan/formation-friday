# GOATs

## Heroes
**Tanks:**
* Reinhardt
* Zarya
* D.Va

**Supports:**
* Moira
* Brigitte
* Lucio

## Summary
GOATs is a 3 tank 3 support heavy sustain composition. It thrives in short range, lacking mid and long range damage. Because of this, GOATs prefers to stick together, find a choke, and smash the other team into a wall.

## Initiation
Because GOATs is a short range composition, the team prefers to stay out of enemy sight until they are ready to engage. Hiding around a corner, ready to smash the other team into a nearby wall. Most initiations will be done by Reinhardt or in some cases Brigitte.

### Combos
**Bubble Charge:** Reinhardt calls "charging, 3, 2, 1" on 1 they drop their shield and charge. Once Reinhardt's shield goes down, Zarya puts a bubble on Reinhardt to keep him from being slept, stunned, or otherwise interrupted out of a pin. 

**Bash Shatter:** Brigitte calls "bashing, 3, 2, 1" and proceeds to CC the hero most likely to cause trouble for Reindhardt's Earthshatter. If the enemy has a Reinhardt, they are the primary target, then Brigitte. If the enemy has neither Reinhardt can often get shatters on their own without a CC opener.

**Graviton Bomb:** Zarya lets D.Va know she's looking for an opening and where she's going to try to land it. In response, D.Va gets ready to throw her Self-Destruct at that location. Both Reinhardt and Brigitte play key roles in ensuring the success. Reinhardt tries to charge the shielded folks, such as Reinhardt, Zarya, Brigitte out of the Graviton Surge. Brigitte tries to bash Lucio before he can use Sound Barrier, or tries to drop the Reindhardt's shield if our Reinhardt misses.

### Follow-up
After/during the opener: Lucio provides an Amped Speed Crossfade, giving the team the ability to collapse on any targets called by Brigitte or Reinhardt. Neither D.Va, Moira or Brigitte should use their movement ability to close the distance, the Lucio speed will get us all there. 

## Ultimates
GOATs thrives in being proactive, this is true in their engagements, in the individual play styles, and finally in ultimate usage. Very rarely should ultimates be used in reaction to the enemy. Only one of the support ultimates can effectively counter the enemy, instead they should be used to ensure GOATs has the advantage early on in a fight and can then clean up the rest with regular abilities.

**Earthshatter**
Most often used as an initiation tool, see Bash Shatter above.

**Graviton Surge**
Can be very effective without using any other ults as long as Zarya is high charge, as well as Brigitte and Reinhardt are still alive to swing into the Graviton Surge. 

Used in combination with Self-Destruct, see Graviton Bomb above.

**Self-Destruct**
Can be very effective to send behind the enemy, forcing them either into our team, or to scatter allowing us to clean up.

Used in combination with Graviton Surge, see Graviton Bomb above.

**Coalescence**
There are two primary uses of Coalescence, first is "win more". We initiate without any ultimates, typically a Bubble Charge. Then as soon as we get our first pick, Moira uses Coalescence to top the team back up ensuring the second and third picks and lowing the chance of losing anyone during them.

Can be used in combination with Graviton Surge if the Graviton gets a good number of people and you are missing Reinhardt to ensure the kills. Be sure to toss a damage orb first to maximize damage.

**Rally**
There are a good number of times that professionals and higher level groups will pop Rally between fights just to apply armor to everyone. Using Rally this way is often for other ult building purpose. Rally can also be used when the fight is turned 5v6 against our favor if one of the other supports goes down.

This ult is not the best counter-ult out there and is better used to sway momentum than to shut down an enemy.

**Sound Barrier**
This is our safety net and the only strong defensive ult. Sound Barrier can cancel or lessen the impact of nearly every other ultimate in the game. That said there are priorities that should be observed. The mostly deadly ultimates on the other team should be identified and it should be saved for those or turning a even fights with both teams missing 1-2.

For example Junkrat counters GOATs, but Lucio's ultimate can counter Junkrat's tire. So about every other tire Junkrat gets Lucio can potentially stop it. This can be enough to keep mediocre Junkrats from hard countering us. Another example is if 2+ folks get caught in a Graviton Surge or Earthshatter, Lucio can keep that ultimate from being effective. 

## Counters
Projectile DPS are the primary source of issues for GOATs. First they counter GOATs by being able to maintain long range and attack from another direction, getting a lot of free damage, especially from high ground that GOATs struggles to contend. Second because GOATs relies on large health pools, 1 main AoE healer, and 2 off AoE healers, the enemy can build ults very quickly. Hanzo, Junkrat, and Pharah all benefit strongly from this and have ults that can easily get at least 2 picks when the team is playing close together. 

This means that while in principle GOATs is countered by those heroes, those heroes have to play to maximize their ult gain and split our attention to actually become a problem. For this reason we should continue to play into our counter for at least a couple team fights before deciding we need to change our heroes.